/**
 * This class showcases the class level data points
 */
public class ClassDefinition {
    /*
       Compiler rules
       *************** 
     1. The java program should be saved with .java extension
     2. The file can contain on ore more class definitions
     3. Convention is to have One class definition per file
     4. Only public and final keyword can be assigned to a class
     5. There can be only on class with the public access specifier per file
     6. The file name should be the same name of the class having the public access 
        specifier
    */
}