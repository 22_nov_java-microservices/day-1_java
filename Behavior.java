/**
 * Java doc for class level
 * Template to store data and behavior
 */
public class Behavior {
    //data and behavior
    // data -> variable store the data
    // methods -> manipulate the data

    // Data 
    //data_type variable_name = value (optional)
    // instance variable
    int counter = 0;

    //instance method
    //method definition
    /*
     access_specifier access_modifier[optional] return_type method_name(arguments [optional]){
        //method body
     }
    */

    public int increment(){
        return counter ++ ;
    }

    public int decrement(){
        return counter --;
    }

    public void printCounter(){
        System.out.println("The current counter value is "+ counter);
    }
}