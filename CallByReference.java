public class CallByReference {

    public static void main(String[] args) {
        
        //data-type variable_name = new DataType();
        CallByReference object = new CallByReference();

        int initialValue = 100;

        int intialArray [] =  {10, 20, 30, 40, 50};

        int result = object.callByValue(initialValue);
        //System.out.println("Initial Value before executing callByValue :: " + initialValue);
        
        //System.out.println("Initial Value after executing callByValue :: " + result);

        //System.out.println("Initial Value after executing callByValue :: " + initialValue);

        //System.out.println("************ Call by reference - start *************");
        
        object.callByReference(intialArray);

        for (int value : intialArray) {
            System.out.println("Initial Value after executing callByReference :: " + value);
        }
        System.out.println("************ Call by reference - end *************");
    }

    public int callByValue(int value){
        value = value * 2;
        return value;
    }

    public void callByReference(int[] array){
        for (int index = 0; index < array.length; index++) {
            array[index] = array[index] * 2;
        }
    }
}