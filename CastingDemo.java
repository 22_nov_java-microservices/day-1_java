public class CastingDemo {

    public static void main(String[] args) {
        //up casting  - For free
        int val = 356;
        long longVal = val;
        float floatVal = val;
        double doubleVal = val;

        // down casting
        long baseValue = 450_458_234234234324L;
        //int intValue = baseValue; // compiler will throw error
        //type casting
        int intValue = (int) baseValue;
        System.out.println("value after downcasting :: "+ intValue);
    }
    
}
