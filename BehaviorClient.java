
// Increment and Decrement demo client
public class BehaviorClient {

    public static void main(String args []){
        //data_type variable_name = value;
                             //No-argument constructor [created by the compiler]    
         Behavior behavior = new Behavior();
         System.out.println("Initial counter value :: "+behavior.counter);
         int counterValue = behavior.increment();
         System.out.println("Counter value after 1st increment :: "+ counterValue);
         counterValue = behavior.decrement();
         System.out.println("Counter value after 1st decrement :: "+ counterValue);
         counterValue = behavior.increment();
         System.out.println("Counter value after 3rd increment :: "+ counterValue);
         counterValue = behavior.increment();
         System.out.println("Counter value after 4th increment :: "+ counterValue);
         counterValue = behavior.increment();
         System.out.println("Counter value after 5th increment :: "+ counterValue);
         counterValue = behavior.increment();
         System.out.println("Counter value after 6th increment :: "+ counterValue);
         behavior.printCounter();
    }
}
