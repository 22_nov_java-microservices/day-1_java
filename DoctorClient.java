import java.util.Scanner;

/*
   - Inheritance is used for making a more specialized verion
   - IS-A relationship
   - Parent-Child is relation
   - Child class acquires the behviour and state using the extends keyword
   - The parent class do not have any details of the child class
   - The child class always have a reference with the super class
   - Using the parent class reference you can only call the methods declared in the parent class
   - The invocation of the method is deferred till the runtime and the invocation is done on the object
   
*/
 abstract class Doctor {
    private String name;
    private int exp;
    public abstract void treatPatient(String name);
}

class OrthoPedician extends Doctor {

    void conductXRay(String name){
        System.out.println("Conducting X-Ray for :: "+name);
    }

     void conductCTScan(String name){
        System.out.println("Conducting CT-Scan for :: "+name);
    }

    @Override
    public void treatPatient(String name){
        conductXRay(name);
        conductCTScan(name);
    }
}

class KneeSurgeon extends OrthoPedician {

    private void conductKneeSurgery(String name){
        System.out.println("Conducting knee surgery :: "+ name);
    }
    @Override    
    public void treatPatient(String name){
        conductXRay(name);
        conductCTScan(name);
        conductKneeSurgery(name);
    }
}
class Padietic extends Doctor {

    public void treatKids(String name){
        System.out.println("Treating Kid :: "+name);
    }

    @Override
    public void treatPatient(String name){
        treatKids(name);
    }
}

public class DoctorClient {
    public static void main(String[] args) {
        //Data-Type ref = new Data-Type();     
        
        //stage - 1
        /*
            Doctor doctor = new Doctor();
            OrthoPedician orthoPedician = new OrthoPedician();
            Padietic padietic = new Padietic();
            // doctor.treatPatient("Ravi");

            //All subclasses inherit the data and behaviour from the super class

            orthoPedician.conductCTScan("Rakesh");
            orthoPedician.conductCTScan("Rakesh");
            orthoPedician.treatPatient("Rakesh");

            padietic.treatKids("Sakshi");
        */
        //stage-2
        //Target      = IS-A
        //compile time       runtime
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the option ");
        System.out.println( " 1 -> Ortho");
        System.out.println( " 2 -> KneeSurgeon");
        System.out.println( " 3 -> Padietric");

        int option = scanner.nextInt();

        Doctor doctor = null;
        switch(option){
            case 1: 
                    doctor = new OrthoPedician();
                    break;
            case 2: 
                    doctor = new KneeSurgeon();
                    break;
            case 3: 
                    doctor = new Padietic();
                    break;
            default: 
                    doctor = new Padietic();
                    break;
        }
//        Doctor doctor = new OrthoPedician();
        Doctor kneeSurgeon = new KneeSurgeon();
        OrthoPedician orthoPedician = new KneeSurgeon();
        //IS-A
        //OrthoPedician doctor12 = new Doctor("Suresh");
        //doctor.treatPatient("Ramesh");

       /*  if (doctor instanceof OrthoPedician) {
            OrthoPedician oPedician = (OrthoPedician)doctor;
            oPedician.conductCTScan("Ramesh");
            oPedician.conductXRay("Ramesh");
        } */
        doctor.treatPatient("Rakesh");
        scanner.close();
    }
}