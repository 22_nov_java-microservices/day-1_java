public class MethodCallDemo {
    public static void main(String[] args) {
        System.out.println("Executing order :: ");
        method1();
        System.out.println("Last statement to be executed");
    }
    private static void method1() {
        System.out.println("Inside the method-1");
        method2();
        System.out.println("After executing method 2");
    }
    private static void method2() {
        System.out.println("Inside the method-2");
        method3();
        System.out.println("After executing method 3");

    }
    private static void method3() {
        System.out.println("Inside method 3");
    }
}
