public class Arrays {

    public static void main(String[] args) {
        // Arrays
        /*
          1. Fixed length  and cannot be changed
          2. Container for storing similar data 
          3. length property for getting the size
          4. Contigous block of memory
          5. Elements can be accessed and modified using index
          6. Index starts with 0 and the last element index will be (length - 1)
        */
        int[] array = new int[5]; // all elements will be having the values set to default value
        System.out.println( " ******************* Array iteration ***********************");
        for ( int index = 0; index < array.length; index ++) {
            System.out.println(" Index : "+ index + " Value: "+ array[index]);
        }
        System.out.println( " ******************* Complete ***********************");
        //default value 
         /*
           int, byte, short, long, double, float -> 0
           boolean -> false
           char -> ''
           objects -> null
         */
        int[] values = new int[]{ 10, 11, 12, 13, 14};

        System.out.println( " ******************* Array iteration ***********************");
        for ( int index = 0; index < values.length; index ++) {
            System.out.println(" Index : "+ index + " Value: "+ values[index]);
        }
    }
}