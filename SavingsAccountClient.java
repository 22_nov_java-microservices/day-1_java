public class SavingsAccountClient {

    public static void main(String[] args) {
        SavingsAccount rameshAcc = new SavingsAccount("Ramesh",  10_000);
        SavingsAccount sureshAcc = new SavingsAccount("Suresh", 10_000);
        SavingsAccount vinayAcc = new SavingsAccount("Vinay");
/*         vinayAcc.setAccountHolderName("Vinay Kumar");
        rameshAcc.checkAccountBalance();
        rameshAcc.deposit(20_000);
        rameshAcc.checkAccountBalance();
        rameshAcc.deposit(30_000);
        rameshAcc.checkAccountBalance();
        rameshAcc.withdraw(10_000);
        rameshAcc.checkAccountBalance();
        vinayAcc.checkAccountBalance();
         *///SavingsAccount strangeConstructor = new SavingsAccount(1234, 44, "Ramesh", "Aravind", 22, true, false);
         System.out.println("Ramesh account number " + rameshAcc.getAccountNumber());
         System.out.println("Suresh account number " + sureshAcc.getAccountNumber());
         System.out.println("Vinay account number " + vinayAcc.getAccountNumber());
    }
}