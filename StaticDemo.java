public class StaticDemo {

    private int notStatic = 100;
    
    private static int staticMember = 100;

    public void nonStaticMethod(){
        System.out.println(" Static Member :: " + staticMember);
        System.out.println(" Static Member :: " + this.staticMember);
        System.out.println(" Static Member :: " + StaticDemo.staticMember);
        System.out.println(" Non Static Member :: " + notStatic);
        System.out.println(" Non Static Member :: " + this.notStatic);
    }
    public static void main(String[] args) {
        /*
            1. A static method can only call another static method/variable
            2. A non static method can access both static and non sttic member
            3. Static members should be accessed in a static manner (Using the class name)
        */
        //System.out.println("Value :: " + nonStatic);
        System.out.println("Value :: " + staticMember);
    }
}