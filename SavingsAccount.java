public class SavingsAccount {

    //data - instance variables
    //instance members -> instance variables + instance methods
    /*
       instance variable default value;
       numbers -> 0
       boolean -> false
       objects -> null
       char -> ''
    */ 
    private static long counter = 1000;

    private long accountNumber;

    private double accountBalance;

    private String accountHolderName;

    //constructor with arguments
    /*
       If we do not supply the constructor -> The compiler will provide a default no arg constructor
       If we provide a constructor -> The compiler will not provide any constructor
       Create multiple constructors with either
         - different number of arguments
         - different data types of arguments
       The constructor resolution will be done at compile done and hence this is called compile time polymorphism  

    */
    public SavingsAccount(String accountHolderName, double accountBalance){
        this.accountHolderName = accountHolderName;
        //should access the static members via the class name.
        this.accountNumber = ++ counter;
        this.accountBalance = accountBalance;
    }    
    
    public SavingsAccount(long accountNumber, String accountHolderName){
        this.accountHolderName = accountHolderName;
        this.accountNumber = ++ counter;
    }   

    public SavingsAccount(String accountHolderName){
        this.accountHolderName = accountHolderName;
        this.accountNumber = ++ counter;
    }
   
    public void setAccountHolderName(String accountHolderName){
        this.accountHolderName  =accountHolderName;
    }

    public String getAccountHolderName(){
        return this.accountHolderName;
    }
    
    //instance methods
    public void deposit(double amount) {
        this.accountBalance = this.accountBalance + amount;
    }

    public void checkAccountBalance(){
        System.out.println("Account Balance of "+this.accountHolderName + " is :: "+this.accountBalance);
    }

    public double withdraw(double amount){
        if ((this.accountBalance - amount) > 0) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public long getAccountNumber(){
        return this.accountNumber;
    }
}