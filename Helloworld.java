/**
 * This class demonstrates
 *  - Comments
 *  - keywords
 *  - data types
 *  - variables
 */
public class Helloworld {

    /**
     * This is a documentation
     * @param args
     * @since 1.0
     * 
     */
    public static void main(String args[]){
        //comments
        // single line comment
        /*
           This is a 
              multi 
              line 
              comment
        */
        //keywords
        /*
           main, for, while, public, private, protected, class
        */
        
        //data
        // num of participants in training - int 
        //num of vacation days in a year - int
        // distance between sun and earth in kms - double
        // population of Asia - long
        // num of cities in Karnataka - int

        //variables
        // Variable -> Placeholder for containing the data
        // Rules for variable naming 
        /*
          1. Cannot user keywords and reserve words as variable names
          2. Can contain only letters and numbers
          3. Only _ and $ are allowed special characters
          4. Cannot start with a digit
          5. Variables are case sensitive
        */
        // Conventions for variable naming
        /*
          1. Class name - Use Capital letter as the first letter
          2. method name and variables - User small letter as the first letter
          3. If the variable name is more than two words, useTheCameCaseNotation
          4. Do not _ as the delimitter 
          5. Use meaningful names as varialbes
             numberOfStudents, index, loop, increment
        */

        //Assigning data to variable 
         /*
           1. Declaring a variable
                dataType variableName;
           2. Assignment
         */
            int numberOfStudents;
            boolean flag;
            char backspace = '\n';
            long numberOfUsers;
            double distanceFromSunToEarth;

            //data assignment
            flag = false;

            //declaration and assignment
            int participantsInBatch = 10;
            distanceFromSunToEarth = 23_00_900.90;
            numberOfUsers = 23_500;
    }
}