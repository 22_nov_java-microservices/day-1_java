
public class MethodOverloading {
    public void greet(String name, String message){
        System.out.println(message + " !! " + name);
    }

    public void greet(String name){
        greet(name, "Hi !! ");
    }

    public static void main(String[] args) {
        MethodOverloading obj = new MethodOverloading();
        obj.greet("Ravi ", "welcome ");
    }
}
